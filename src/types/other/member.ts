type Member = {
    id: number,
    name: string,
    tel: string,
    points: number
}

export{type Member}