import type { BuyMaterial } from './BuyMaterial'
type ReceiptMaterialItem = {
  id: number
  name: string
  unit: number
  price: number
  stockId: number
  buyMaterial: BuyMaterial
}

export { type ReceiptMaterialItem }
