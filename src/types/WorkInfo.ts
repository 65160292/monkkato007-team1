import type { User } from "./other/User";

type WorkInfo = {
    id: number
    email: string
    timeIn?: Date
    timeOut?: Date
};

// เข้ารหัสข้อมูลเพื่อดึงค่า email จาก User ตัวแรกใน user[]
function extractUserEmail(user?: User[]): string | undefined {
    return user?.[0]?.email;
}

export { type WorkInfo };
