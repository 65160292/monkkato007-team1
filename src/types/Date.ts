type Date = {
    day: 'numeric'
    month: 'numeric'
    year: 'numeric'
    hour: 'numeric'
    minute: 'numeric'
    hour12: false // Use 24-hour format
  }
  export type { Date }