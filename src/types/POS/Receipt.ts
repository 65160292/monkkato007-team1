import type { User } from "../other/User"
import type { Member } from "../other/member"
import type { ReceiptItem } from "./ReceiptItem"
type PaymentType = "Cash"|"Promptpay"|"Credit card"
type Receipt ={
    id: number,
    createDate: Date ,
    subtotal: number,
    total: number,
    receivedAmount: number,
    change: number,
    paymentType:  PaymentType[],
    userId: number,
    user?: User,
    memberId: number,
    member?: Member,
    discount: number,
    receiptItems?: ReceiptItem[]
}

export {type Receipt}