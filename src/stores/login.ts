import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoginStore = defineStore('Login', () => {
  const loginEmail = ref("")
  const isLogin = computed(() => {
    return loginEmail.value !== "";
  })
  
  const login = (userName: string): void => {
    loginEmail.value = userName;
    localStorage.setItem("loginEmail", userName)
  }
  const logout = () => {
    loginEmail.value = ""
    console.log("asdasd")
    localStorage.removeItem("loginEmail")
  }
  const loadData = () => {
    loginEmail.value = localStorage.getItem("loginEmail") || ""
  }
  return { loginEmail, isLogin, login,logout, loadData}
})
