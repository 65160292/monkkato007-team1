import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/POS/ReceiptItem'
import type { Product } from '@/types/POS/Product'
import type { Receipt } from '@/types/POS/Receipt'
import { useAnthStore } from './anth'
import { useMemberStore } from './member'

export const useReceiptStore = defineStore('POS', () => {
  const tel = ref('')
  const anthStore = useAnthStore()
  const receiptDialog = ref(false)
  const paymentCash = ref(false)
  const paymentQR =  ref(false)
  const paymentCredit = ref(false)
  const memberStore = useMemberStore()
  const receipt = ref<Receipt>({
    id: 0,
    createDate: new Date,
    subtotal: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: ['Cash'],
    userId: anthStore.createUser.id,
    user: anthStore.createUser,
    memberId: 0,
    discount: 0
})
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItems(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: '-1',
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }
  function removeReceiptItems(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItems(item)
    }
    item.unit--
    calReceipt()
  }
  function calReceipt(){
    let subtotal = 0;
    for(const item of receiptItems.value){
      subtotal = subtotal + (item.price * item.unit)
    }
    receipt.value.subtotal = subtotal
    if(memberStore.currentMember){
      receipt.value.total = subtotal *0.95
      receipt.value.discount = subtotal - receipt.value.total
    }else{
      receipt.value.total = subtotal
    }
  }
  const selectedPayment = ref('')
  function showreceiptDialog( selectedPayment : string){
    const item = receiptItems.value.length
    if(item === 0){
      return
    }else{
      if(selectedPayment === 'Cash'){
        paymentCash.value = true
      }else if(selectedPayment === 'Promptpay'){
        paymentQR.value = true
      }else if(selectedPayment === 'Credit card'){
        paymentCredit.value = true
      }else{
        return
      }
    }
  }
  function showbill(){
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }
  function clearReceipt(){
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createDate: new Date,
      subtotal: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: [],
      userId: anthStore.createUser.id,
      user: anthStore.createUser,
      memberId: 0,
      discount: 0
    }
    tel.value=''
    memberStore.clear()
  }
  function calchange(){
    if(receipt.value.receivedAmount - receipt.value.total > 0){
      receipt.value.change = receipt.value.receivedAmount - receipt.value.total
    }else if (receipt.value.receivedAmount - receipt.value.total < 0){
      receipt.value.change = 0
      return
    }
  }
  return { receiptItems,receipt,receiptDialog,selectedPayment,paymentCash,paymentQR,paymentCredit,tel,
    addReceiptItems, removeReceiptItems, inc, dec,calReceipt,showreceiptDialog,clearReceipt,showbill,calchange}})