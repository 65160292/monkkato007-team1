import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { BuyMaterial } from '@/types/Stock/BuyMaterial'
export const useBuyMaterial = defineStore('buyMaterial', () => {
  const buymat = ref<BuyMaterial[]>([
    {
        id: 1,
        name: 'coffee beans',
        price: 300,
        unit: 'kg',
        quantity: 0
    },
    {
        id: 2,
        name: 'green tea powder',
        price: 250,
        unit: 'kg',
        quantity: 0
    },
    {
        id: 3,
        name: 'Thai tea powder',
        price: 240,
        unit: 'kg',
        quantity: 0
    },
    {
        id: 4,
        name: 'Milk',
        price: 100,
        unit: 'liter',
        quantity: 0
    },
    {
        id: 5,
        name: 'Water',
        price: 50,
        unit: 'liter',
        quantity: 0
    },
    {
        id: 6,
        name: 'suger',
        price: 50,
        unit: 'kg',
        quantity: 0
    },
    {
        id: 7,
        name: 'Cocoa powder',
        price: 260,
        unit: 'kg',
        quantity: 0
    },
    {
        id: 8,
        name: 'Syrup',
        price: 80,
        unit: 'liter',
        quantity: 0
    },
    {
        id: 9,
        name: 'Sweetened condensed milk',
        price: 120,
        unit: 'liter',
        quantity: 0
    }
])

  return { buymat }
})