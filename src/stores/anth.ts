import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/other/User'

export const useAnthStore = defineStore('anth', () => {
  const createUser = ref<User>({
    id: 1,
    email: 'my@buu.ac.th',
    password: '1122',
    fullName: 'บุญดี มีญาติ',
    gender: 'male',
    roles: ['user']
})

  return { createUser }
})
