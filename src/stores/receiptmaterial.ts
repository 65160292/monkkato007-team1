import { ref, computed, provide } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptMaterialItem } from '@/types/Stock/ReceiptMaterialItem'
import type { BuyMaterial } from '@/types/Stock/BuyMaterial'
import type { ReceiptMaterial } from '@/types/Stock/ReceiptMaterial'
import { useMemberStore } from './member'


export const useReceiptMaterialStore = defineStore('receiptMaterial', () => {
    const memberStore = useMemberStore()
    const recieptDialog = ref(false)

    const receipt = ref<ReceiptMaterial>({
        id: 0,
        createdDate: new Date(),
        totalBefore: 0,
        createdDateString: '',
        total: 0,
        change: 0,
        paymentType: 'Cash',
        receiptStockItem: []
    })
    const receiptMaterialItems = ref<ReceiptMaterialItem[]>([])
    function addReceiptMaterialItem(buyMaterial: BuyMaterial) {
        const index = receiptMaterialItems.value.findIndex((item) => item.buyMaterial?.id === buyMaterial.id)
        if (index >= 0) {
            receiptMaterialItems.value[index].unit++
            calReceipt()
            return
        } else {
            const newReceipt: ReceiptMaterialItem = {
                id: -1,
                name: buyMaterial.name,
                price: buyMaterial.price,
                unit: 1,
                stockId: buyMaterial.id,
                buyMaterial: buyMaterial
            }
            receiptMaterialItems.value.push(newReceipt)
            calReceipt()
        }
    }
    function removeReceiptItem(receiptMaterialItem: ReceiptMaterialItem) {
        const index = receiptMaterialItems.value.findIndex((Item) => Item === receiptMaterialItem)
        receiptMaterialItems.value.splice(index, 1)
        calReceipt()
    }
    function inc(item: ReceiptMaterialItem) {
        item.unit++
        calReceipt()
    }
    function dec(item: ReceiptMaterialItem) {
        if (item.unit == 1) {
            removeReceiptItem(item)
        }
        item.unit--
        calReceipt()
    }
    function calReceipt() {
        let totalBefore = 0
        for (const item of receiptMaterialItems.value) {
            totalBefore = totalBefore + (item.price * item.unit)
        }
        receipt.value.totalBefore = totalBefore
        receipt.value.total = totalBefore
        if (memberStore.currentMember != null) {
            receipt.value.total = totalBefore * 0.95
        }
        else {
            receipt.value.total = totalBefore
        }
    }
    function showRecieptDialog() { 
        recieptDialog.value = true
    }
    function clear() {
        receiptMaterialItems.value = []
        receipt.value = {
            id: 0,
            createdDate: new Date(),
            totalBefore: 0,
            createdDateString: '',
            total: 0,
            change: 0,
            paymentType: 'Cash',
            receiptStockItem: []
        }
        memberStore.clear()
    }
    return {
        receiptMaterialItems, receipt,
        addReceiptMaterialItem, removeReceiptItem, inc, dec, calReceipt, recieptDialog, showRecieptDialog, clear
    }
})